import React, { Component } from 'react';
import "./form.css"
import validator from 'validator';


class Form extends Component {
    constructor(){
        super();
        this.state={
            firstName:"",
            lastName:"",
            email:"",
            age:'',
            password:"",
            repeatPassword:"",
            terms:false,
            errors:{}
        }
    }
    

    validate= ()=>{

        const {firstName,lastName,password,repeatPassword,email,age,terms}=this.state
        const errors={}
        if(!validator.isAlpha(firstName)){
            errors.firstName="First name must not contain numbers"
            
        }
        if(!validator.isAlpha(lastName)){
            errors.lastName="Last name must not contain numbers"
        }

        if(!validator.isInt(age,{min:18,max:70})){
            errors.age="Age Should be in between 18 and 70"
        }
        if(password==="" || password.length<6){
            errors.password="Please enter a password at least 6 characters long"
        }
        if(repeatPassword!==password || password===""){
            errors.repeatPassword="Don't leave this field blank and both the passwords must be same"
        }
        if (!validator.isEmail(email)){
            errors.email="Please enter a valid email"
        }

        if(terms!==true){
            errors.terms="Please agree to our terms and conditions"
        }
        return Object.keys(errors).length===0? null: errors

    }

    handelSubmit=(event)=>{
        event.preventDefault()
        const errors=this.validate()
        this.setState({errors:errors  || {}})
        if(errors) return
        console.log("submitted");
        
    }

    handleChange=(event)=>{
        
        if(event.target.name==="fname"){
            this.setState(
                {firstName:event.target.value}
            )
            // console.log(event.target.value);
        }
        if(event.target.name==="lname"){
            this.setState(
                {lastName:event.target.value}
            )
        }

        if(event.target.name==="age"){
            this.setState(
                {age:event.target.value}
            )
        }
        if(event.target.name==="email"){
            this.setState(
                {email:event.target.value}
            ) 
        }
        if(event.target.name==="password"){
            this.setState(
                {password:event.target.value}
                )
        }
        if(event.target.name==="repeatPassword"){
            this.setState(
                {
                    repeatPassword:event.target.value
                }
            )
        }
        if(event.target.name==="terms"){
            this.setState(
                {
                    terms:true
                }
            )
        }
    }

    render() {
        return (
            <div className='container'>
                <div className='signUpContainer'>
                 <div className='leftSection'>
                    <form onSubmit={this.handelSubmit}>
                        <h1>Sign Up here</h1>
                        <label htmlFor="fname"></label>
                        <input placeholder='First Name' type="text" id="fname" name="fname" onChange={this.handleChange} value={this.state.firstName} />
                        {this.state.errors.firstName && <div className='errorMessage'>{this.state.errors.firstName}</div> }
                        <label htmlFor="lname"></label>
                        <input placeholder='Last name' type="text" id="lname" name="lname" onChange={this.handleChange} value={this.state.lastName}/>
                        {this.state.errors.lastName && <div className='errorMessage'>{this.state.errors.lastName}</div> }
                        <label htmlFor="age"></label>
                        <input placeholder='age' type="text" id="age" name="age" onChange={this.handleChange} value={this.state.age} />
                        {this.state.errors.age && <div className='errorMessage'>{this.state.errors.age}</div> }
                        <label htmlFor="gender">Gender</label>
                        <select name="gender" id="gender">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="other">Others</option>
                        </select>
                        <label htmlFor="Role">Role</label>
                        <select name="role" id="role">
                            <option value="male">Developer</option>
                            <option value="female">Senior Developer</option>
                            <option value="other">Lead Engineer</option>
                            <option value="other">CTO</option>
                        </select>
                        <label htmlFor="Email"></label>
                        <input placeholder='email' type="text" id="email" name="email" onChange={this.handleChange} value={this.state.email}/>
                        {this.state.errors.email && <div className='errorMessage'>{this.state.errors.email}</div> }
                        <label htmlFor="password"></label>
                        <input placeholder='Password' type="password" id="password" name="password" onChange={this.handleChange} value={this.state.password}/>
                        {this.state.errors.password && <div className='errorMessage'>{this.state.errors.password}</div> }
                        <label htmlFor="repeatPassword"></label>
                        <input placeholder='Repeat Password' type="password" id="repeatPassword" name="repeatPassword" onChange={this.handleChange} value={this.state.repeatPassword}/>
                        {this.state.errors.repeatPassword && <div className='errorMessage'>{this.state.errors.repeatPassword}</div> }
                        <div className="checkbox">
                               
                            <input type="checkbox" id="terms" name="terms" onChange={this.handleChange} value={this.state.terms}/>
                                <p> Agree to our terms and conditions </p>
                        </div>
                        {this.state.errors.terms && <div className='errorMessage'>{this.state.errors.terms}</div> }
                        <button type="submit">Sign Up</button>
                    </form>
                 </div>
                 <div className='rightSection'></div>
                </div>

            </div>
        );
    }
}

export default Form;
